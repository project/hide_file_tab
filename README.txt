CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration


INTRODUCTION
------------

Hide file tab is used to remove the file tab from view on the /admin/content
section.

The motivation for this is due to the confusion between the File tab and Media
tab (when enabled). This could be solved with the use of the 'Access the Files
overview page' permission however it cannot be removed for the admin role.

Note: this will only hide the tab and does not restrict access to it for users
that actually have the 'Access the Files overview page' permission.


INSTALLATION
------------

For installation of the hide_file_tab module please follow the steps:

• Install as usual
• Enable the hide_file_tab module.


CONFIGURATION
-------------

• There is no configuration for this module.
